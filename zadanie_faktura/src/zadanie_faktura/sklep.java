package zadanie_faktura;



public class sklep {

	public static void main(String[] args) {
		
		Invoice invoice1 = new Invoice();
		Invoice invoice2 = new Invoice();
		
		invoice1.towar.add(new towar("Czekolada Milka", "sztuk", 5, 20, 18));
		invoice1.towar.add(new towar("Mleko 2%", "sztuk", 3, 30, 15));
		invoice1.towar.add(new towar("Sok Tymbark", "sztuk", 2, 23, 18));
		
		invoice1.calculate_and_print();
	
		invoice2.towar.add(new towar("Zeszyt w kratke", "sztuk", 5, 20, 15));
		invoice2.towar.add(new towar("Dlugopisy", "sztuk", 1.6, 100, 13));
		invoice2.towar.add(new towar("Pomarancze", "kg", 10, 28, 18));
		
		invoice2.calculate_and_print();

		
	}

}